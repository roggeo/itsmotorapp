
## Install Gandle

## Install React Native

    $ npx react-native init ItsMotor

## Enable usesClearTextTraffic within AndroidManifest.xml

android:usesCleartextTraffic="true"


## Enable USB debugging in your phone

Into settings -> Developer -> USB Debugging

    $  adb devices

## Run Android

react-native start
react-native run-android


