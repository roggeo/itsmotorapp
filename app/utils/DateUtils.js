export function getMonthName(num) {
    const monthNames = ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro']
    if (monthNames[num-1]) {
        return monthNames[num-1];
    }
    return num;
}

export function getDateBrFormat(dt) {
    let val = dt.split('-');
    return val[2] +'/'+ val[1] +'/'+ val[0];
}