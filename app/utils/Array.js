export function arrayChunk(array, size) {
    if (!array) return [];
    const firstChunk = array.slice(0, size);
    if (!firstChunk.length) {
        return array;
    }
    return [firstChunk].concat(arrayChunk(array.slice(size, array.length), size)); 
}