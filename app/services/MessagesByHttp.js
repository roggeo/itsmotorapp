const MESSAGES = {
    schedule_confirmed_1: [
        'Enviado',
        'Agendamento enviado com sucesso!'
    ],
    no_message_found: [
        'Alerta',
        'Mensagem inexistente'
    ]
}

export function getMessageByHttp(msg) {
    if (msg in MESSAGES) {
        return MESSAGES[msg];
    }
    return MESSAGES.no_message_found
}