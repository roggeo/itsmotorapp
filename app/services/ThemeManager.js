import Dark from './../themes/dark';

export function DarkTheme() {
    return new Dark();
}