import Logo from './itsmotor_mob_logo.png';
import MapsMarker from './mapmarker.png';
import NoImageCompany from './no-img-company.svg.png';
import NoImageVehicle from './no-img-vehicle.svg.png';

export default class IndexImages {
    logo() {
        return Logo
    }
    noImageCompany() {
        return NoImageCompany
    }
    mapsMarker() {
        return MapsMarker
    }
    noImageVehicle() {
        return NoImageVehicle
    }
}