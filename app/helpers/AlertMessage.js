import React from 'react';
import {
    View,
    Text,
    StyleSheet
} from 'react-native'

export const AlertInfo = (props) => {

    const context = props.children;
    
    return (<>
        <View style={Style.containerInfo}>
            <Text style={Style.textInfo}>{context}</Text>
        </View>
    </>);
}

const Style = StyleSheet.create({
  containerInfo: {
    padding: 10,
    borderRadius: 5,
    backgroundColor: '#2089dc'
  },
  textInfo: {
    textAlign: 'justify',
    color: 'white'
  }
});