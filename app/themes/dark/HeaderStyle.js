import { StyleSheet } from 'react-native';
import Colors from './Colors';

const HeaderStyle = StyleSheet.create({
    headerView: {
        paddingVertical: 10,        
        backgroundColor: Colors.colorBodyBg,
        flexDirection: 'row',
    },
    logo: {
        width:120,
        height:20,
        marginLeft:15,
        marginTop:8
    },
    iconContainer: {
        marginLeft:10,
    }
});

export default HeaderStyle;