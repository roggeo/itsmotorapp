const Colors = {
    colorBodyBg: '#222222',
    colorBodyScreenBg: '#ffffff',
    colorTopBarBg: '#333333',
    colorSideMenuIcon: '#999999',
    colorSideMenuText: '#ffffff',
    colorTextBody: '#999999',
    form: {
        colorIcon: '#27a566',
        colorInput: '#ffffff',
        colorPlaceholder: '#888888',
        colorBtnBg: '#27a566',
        colorBtnText: '#000',
        colorInputBorder: '#888888',
    }
}

export default Colors;