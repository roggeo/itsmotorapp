import { StyleSheet } from 'react-native';
import Colors from './Colors';

const NavMenuStyle = StyleSheet.create({
    navContainer: {
        paddingVertical: 10,        
        backgroundColor: Colors.colorBodyBg,
        flexDirection: 'row',
    },
    logo: {
        // width:110,
        // height:20
    },
    logoTitleContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    iconContainer: {
        marginLeft:10,
    }
});

export default NavMenuStyle;