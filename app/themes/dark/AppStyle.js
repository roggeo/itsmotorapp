import { StyleSheet } from 'react-native';
import Colors from './Colors';

const AppStyle = StyleSheet.create({
    containerSafeArea: {
        flex: 1
    },
    body: {
        backgroundColor: Colors.colorBodyBg,
    }    
});
export default AppStyle;