import { StyleSheet } from 'react-native';
import Colors from './Colors';

const SideMenuStyle = StyleSheet.create({
    container: {
        backgroundColor: Colors.colorBodyBg,
    },
    containerSafeArea: {
        flex: 1,
        paddingTop: 15
    },
    menuLinkRow: {
        flex: 1,
        paddingLeft: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        marginVertical: 10
    },
    menuLinkText: {
        color: Colors.colorSideMenuIcon,
        marginLeft: 10,
        fontSize: 18
    }
});

export default SideMenuStyle;