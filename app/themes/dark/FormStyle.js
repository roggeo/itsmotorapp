import { StyleSheet } from 'react-native';
import Colors from './Colors';

var FormStyle = {
    default: StyleSheet.create({
        input: {
            color: Colors.form.colorInput,
        },
        inputLight: {
            color: '#555555'
        },
        textInputLight: {
            color: '#555555',
            fontSize: 18
        },
        labelLight: {
            color: '#75847c',
            fontSize: 16,
            fontWeight: 'bold',
        },
        labelDescLight: {
            color: '#75847c',
            fontSize: 16,
            fontWeight: 'bold',
            marginLeft: 12
        },
        textAreaDescLight: {
            color: '#555555',
            fontSize: 18,
            marginHorizontal: 9
        },
        inputContainer: {
            borderColor:  Colors.form.colorInputBorder,
            borderBottomWidth: 1,
        },
        inputContainerStyle: {
            borderBottomWidth: 0,
        },
        formRow: {
            flex:1,
            marginHorizontal: 9,
            marginVertical: 12
        },
        button: {
            backgroundColor: Colors.form.colorBtnBg,
        },
        buttonTitle: {
            color: '#ffffff',
            textTransform: 'uppercase',
        },
        errors: {
            color: 'red',
            fontSize: 16
        }
    })
};

export default FormStyle;
