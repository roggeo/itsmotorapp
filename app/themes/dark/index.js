import Colors from './Colors';
import AppStyle from './AppStyle';
import FormStyle from './FormStyle';
import FooterStyle from './FooterStyle';
import HeaderStyle from './HeaderStyle';
import NavMenuStyle from './NavMenuStyle';
import SideMenuStyle from './SideMenuStyle';

export default class DarkTheme {
    colors() {
        return Colors
    }
    
    appStyle() {
        return AppStyle
    }
    
    formStyle() {
        return FormStyle
    }
    
    headerStyle() {
        return HeaderStyle
    }

    footerStyle() {
        return FooterStyle
    }
    
    navMenuStyle() {
        return NavMenuStyle
    }
    
    sideMenuStyle() {
        return SideMenuStyle
    }
}