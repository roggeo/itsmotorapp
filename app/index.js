import React from 'react';
import { Dimensions } from 'react-native';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createAppContainer } from 'react-navigation';

import SideMenu from './components/menu/SideMenu';
import StackNav from './components/routers/StackNav';

const DrawerNav = createDrawerNavigator({
    Nav1: {
        screen: StackNav,
        navigationOptions: {
          drawerLabel: 'Main Menu',
        },
      }
    },
    {
      drawerType: 'slide',
      drawerPosition: 'left',
      contentComponent: SideMenu,
      drawerWidth: Dimensions.get('window').width - 120,
    }
);

export default createAppContainer(DrawerNav);