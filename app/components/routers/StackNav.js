import React from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
} from 'react-native';

import { createStackNavigator } from 'react-navigation-stack';
import { Icon } from 'react-native-elements';
import { DarkTheme } from './../../services/ThemeManager';
import { AssetsImages } from './../../services/AssetsManager';

// Screens
import HomeScreen from './../screens/HomeScreen';
import MessagesScreen from "./../screens/MessagesScreen";
import AccountScreen from "./../screens/AccountScreen";
import ServicesScreen from "./../screens/ServicesScreen";
import VehiclesScreen from "./../screens/VehiclesScreen";
import SearchResultScreen from "./../screens/SearchResultScreen";
import AdvancedSearchScreen from "./../screens/AdvancedSearchScreen";
import ScheduleVehicleScreen from "./../screens/ScheduleVehicleScreen"
import ScheduleDescriptionScreen from "./../screens/ScheduleDescriptionScreen";
import ScheduleCalendarScreen from "./../screens/ScheduleCalendarScreen";
import MessageScreen from '../screens/MessageScreen';
import VehicleScreen from '../screens/VehicleScreen';
import ExampleScreen from '../screens/ExampleScreen';
import ServiceScreen from '../screens/ServiceScreen';
import SchedulingsScreen from '../screens/SchedulingsScreen';
import SchedulingScreen from '../screens/SchedulingScreen';
import FuelBillListScreen from '../screens/FuelBillListScreen';
import FuelBillScreen from '../screens/FuelBillScreen';

const { navMenuStyle, colors } = DarkTheme();
const { logo } = AssetsImages();


const stackNav = createStackNavigator(
  {
    Home : {
      screen: HomeScreen,
      navigationOptions: ({navigation}) => ({
          headerTitle: () => (
            <View style={{flex: 1, width: 90,height: 25, alignContent: 'flex-start'}}>
              <Image
                style={{width: '100%', height: '100%'}}
                source={logo()}
              />
            </View>
          ),
          headerTitleContainerStyle: navMenuStyle().logoTitleContainer,
          headerLeft: () => (
            <TouchableOpacity
              onPress={() => navigation.openDrawer()}
              style={navMenuStyle().iconContainer}
            >
              <Icon
                  name='menu'
                  type='material'
                  color={colors().form.colorIcon}
                  size={35}
              />
            </TouchableOpacity>
          ),
          headerRight: () => (
            <TouchableOpacity
              onPress={() => navigation.navigate('FuelBillList')}
            >
              <Icon
                iconStyle={{marginRight: 25}}
                name='local-gas-station'
                type='material'
                color={colors().form.colorIcon}
                size={35}
              />
            </TouchableOpacity>
          ),
          headerStyle: {
            backgroundColor: colors().colorBodyBg,
          },
          headerTintColor: colors().colorSideMenuText,
          cardStyle: {
            backgroundColor: colors().colorBodyBg
          },
      })
    },
    Account: {
      screen: AccountScreen,
      navigationOptions: ({navigation}) => ({
          title: "Minha conta",
          headerStyle: {
            backgroundColor: colors().colorBodyBg,
          },
          headerTintColor: colors().colorSideMenuText,
          cardStyle: {
            backgroundColor: colors().colorBodyScreenBg
          },
      })
    },
    Messages: {
      screen: MessagesScreen,
      navigationOptions: ({navigation}) => ({
          title: "Mensagens",
          headerStyle: {
            backgroundColor: colors().colorBodyBg,
          },
          headerTintColor: colors().colorSideMenuText,
          cardStyle: {
            backgroundColor: colors().colorBodyScreenBg
          },
      })
    },
    Schedulings: {
      screen: SchedulingsScreen,
      navigationOptions: ({navigation}) => ({
          title: "Agendamentos",
          headerStyle: {
            backgroundColor: colors().colorBodyBg,
          },
          headerTintColor: colors().colorSideMenuText,
          cardStyle: {
            backgroundColor: colors().colorBodyScreenBg
          },
      })
    },
    Services: {
      screen: ServicesScreen,
      navigationOptions: ({navigation}) => ({
          title: "Meus serviços",
          headerRight: () => (
            <TouchableOpacity
              onPress={() => navigation.navigate('Service')}
            >
              <Icon
                iconStyle={{marginRight: 25}}
                name='add-circle'
                type='material'
                color={colors().form.colorIcon}
                size={35}
              />
            </TouchableOpacity>
          ),
          headerTintColor: colors().colorSideMenuText,
          headerStyle: {
            backgroundColor: colors().colorBodyBg,
          },          
          cardStyle: {
            backgroundColor: colors().colorBodyScreenBg
          },
      })
    },
    Vehicles: {
      screen: VehiclesScreen,
      navigationOptions: ({navigation}) => ({
          title: "Meus veículos",
          headerRight: () => (
            <TouchableOpacity
              onPress={() => navigation.navigate('Vehicle')}
            >
              <Icon
                iconStyle={{marginRight: 25}}
                name='add-circle'
                type='material'
                color={colors().form.colorIcon}
                size={35}
              />
            </TouchableOpacity>
          ),
          headerTintColor: colors().colorSideMenuText,
          headerStyle: {
            backgroundColor: colors().colorBodyBg,
          },          
          cardStyle: {
            backgroundColor: colors().colorBodyScreenBg
          },
      })
    },
    FuelBillList: {
      screen: FuelBillListScreen,
      navigationOptions: ({navigation}) => ({
          title: "Combustível",
          headerStyle: {
            backgroundColor: colors().colorBodyBg,
          },
          headerRight: () => (
            <TouchableOpacity
              onPress={() => navigation.navigate('FuelBill')}
            >
              <Icon
                iconStyle={{marginRight: 25}}
                name='add-circle'
                type='material'
                color={colors().form.colorIcon}
                size={35}
              />
            </TouchableOpacity>
          ),
          headerTintColor: colors().colorSideMenuText,
          cardStyle: {
            backgroundColor: colors().colorBodyScreenBg
          },
      })
    },
    SearchResult: {
      screen: SearchResultScreen,
      navigationOptions: ({navigation}) => ({
          title: "Resultado da pesquisa",
          headerStyle: {
            backgroundColor: colors().colorBodyBg,
          },
          headerTintColor: colors().colorSideMenuText,
          cardStyle: {
            backgroundColor: colors().colorBodyScreenBg
          },
      })
    },
    AdvancedSearch: {
      screen: AdvancedSearchScreen,
      navigationOptions: ({navigation}) => ({
          title: "Pesquisa Avançada",
          headerStyle: {
            backgroundColor: colors().colorBodyBg,
          },
          headerTintColor: colors().colorSideMenuText,
          cardStyle: {
            backgroundColor: colors().colorBodyScreenBg
          },
      })
    },
    ScheduleVehicle: {
      screen: ScheduleVehicleScreen,
      navigationOptions: ({navigation}) => ({
          title: "Escolha o veículo (2/3)",
          headerStyle: {
            backgroundColor: colors().colorBodyBg,
          },
          headerTintColor: colors().colorSideMenuText,
          cardStyle: {
            backgroundColor: colors().colorBodyScreenBg
          },
      })
    },
    ScheduleDescription: {
      screen: ScheduleDescriptionScreen,
      navigationOptions: ({navigation}) => ({
          title: "Descreva o problema (3/3)",
          headerStyle: {
            backgroundColor: colors().colorBodyBg,
          },
          headerTintColor: colors().colorSideMenuText,
          cardStyle: {
            backgroundColor: colors().colorBodyScreenBg
          },
      })
    },
    ScheduleCalendar: {
      screen: ScheduleCalendarScreen,
      navigationOptions: ({navigation}) => ({
          title: "Escolha o dia e horário (1/3)",
          headerStyle: {
            backgroundColor: colors().colorBodyBg,
          },
          headerTintColor: colors().colorSideMenuText,
          cardStyle: {
            backgroundColor: colors().colorBodyScreenBg
          },
      })
    },
    Message: {
      screen: MessageScreen,
      navigationOptions: ({navigation}) => ({
          title: navigation.getParam('name'),
          headerStyle: {
            backgroundColor: colors().colorBodyBg,
          },
          headerTintColor: colors().colorSideMenuText,
          cardStyle: {
            backgroundColor: colors().colorBodyScreenBg
          },
      })
    },
    Vehicle: {
      screen: VehicleScreen,
      navigationOptions: ({navigation}) => ({
          title: navigation.getParam('title') || 'Adicionar Veículo',
          headerStyle: {
            backgroundColor: colors().colorBodyBg,
          },
          headerTintColor: colors().colorSideMenuText,
          cardStyle: {
            backgroundColor: colors().colorBodyScreenBg
          },
      })
    },
    Service: {
      screen: ServiceScreen,
      navigationOptions: ({navigation}) => ({
          title: navigation.getParam('id') ? 'Serviço' : 'Adicionar Serviço',
          headerStyle: {
            backgroundColor: colors().colorBodyBg,
          },
          headerTintColor: colors().colorSideMenuText,
          cardStyle: {
            backgroundColor: colors().colorBodyScreenBg
          },
      })
    },
    Scheduling: {
      screen: SchedulingScreen,
      navigationOptions: ({navigation}) => ({
          title: 'Agendamento',
          headerStyle: {
            backgroundColor: colors().colorBodyBg,
          },
          headerTintColor: colors().colorSideMenuText,
          cardStyle: {
            backgroundColor: colors().colorBodyScreenBg
          },
      })
    },
    FuelBill: {
      screen: FuelBillScreen,
      navigationOptions: ({navigation}) => ({
          title: navigation.getParam('id') ? 'Detalhe combustível' : 'Adicionar combustível',
          headerStyle: {
            backgroundColor: colors().colorBodyBg,
          },
          headerTintColor: colors().colorSideMenuText,
          cardStyle: {
            backgroundColor: colors().colorBodyScreenBg
          },
      })
    },
    Example: {
      screen: ExampleScreen,
      navigationOptions: ({navigation}) => ({
          title: "Exemplo de tela",
          headerStyle: {
            backgroundColor: colors().colorBodyBg,
          },
          headerTintColor: colors().colorSideMenuText,
          cardStyle: {
            backgroundColor: colors().colorBodyScreenBg
          },
      })
    },
  }
);

export default stackNav;