import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput
} from 'react-native';
import { Formik } from 'formik';
import {
    Icon,
    Input,
    Button,
    Divider
} from 'react-native-elements';
import { DarkTheme } from './../../services/ThemeManager';
import ModalAlert from '../modal/ModalAlert';
import * as yup from 'yup';

const FormValidation = () => (
    yup.object().shape({
        serviceCode: yup
            .string(),
        serviceName: yup
            .string()
            .required('Nome do serviço é necessário.'),
        serviceVehicleId: yup
            .number('Digite código do veículo.')
            .required('Selecione o veículo.'),
        serviceMechanical: yup
            .string()
            .required('Nome do mecânico é necessário.'),
        serviceDateStart: yup
            .string()
            .required('A data de início do serviço é necessário.'),
        serviceDateFinish: yup
            .string(),
        serviceVehicleKm: yup
            .string()
            .required('A kilometragem do veículo é necessário.'),
        serviceReplacedPieces: yup
            .string('Nome das peças deve em texto'),
        serviceDescription: yup
            .string('Descrição deve ser texto'),
    })
);

const ServiceForm = (props) => {

    const [loading, setLoading] = useState(true);
    const [btnLoading, setBtnLoading] = useState(false);
    const [modalIsVisible, setModalIsVisible] = useState(false);
    const { navigation } = props;
    const { formStyle, colors } = DarkTheme();
    const initValues = {
        serviceCode: '',
        serviceName: '',
        serviceVehicleId: '',
        serviceMechanical: '',
        serviceDateStart: '',
        serviceDateFinish: '',
        serviceVehicleKm: '',
        serviceReplacedPieces: '',
        serviceDescription: '',
    };

    const handleFormSubmit = async (values) => {
       setBtnLoading(true);
       setModalIsVisible(true);
        console.log('salvar dados do veículo');
    }

    const handleOnModalAlertSuccess = () => {
        setModalIsVisible(false);
        navigation.navigate('Services');
    }

    return (
        <>
        <Formik
        initialValues={initValues}
        onSubmit={handleFormSubmit}
        validationSchema={FormValidation()}
        >
        {({ handleChange, handleBlur, handleSubmit, values, errors }) => (
            <View>
                <View style={formStyle().default.formRow}>
                    <Input
                        name='serviceCode'
                        onChangeText={handleChange('serviceCode')}
                        onBlur={handleBlur('serviceCode')}
                        value={values.serviceCode}
                        label='Código'
                        labelStyle={formStyle().default.labelLight}
                        placeholder='Digite aqui...'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        errorMessage={errors.serviceCode || null}
                        errorStyle={formStyle().default.errors}
                    />
                </View>
                <View style={formStyle().default.formRow}>
                    <Input
                        name='serviceName'
                        onChangeText={handleChange('serviceName')}
                        onBlur={handleBlur('serviceName')}
                        value={values.serviceName}
                        label='Serviço'
                        labelStyle={formStyle().default.labelLight}
                        placeholder='Digite aqui...'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        errorMessage={errors.serviceName || null}
                        errorStyle={formStyle().default.errors}
                    />
                </View>
                <View style={formStyle().default.formRow}>
                    <Input
                        name='serviceVehicleId'
                        onChangeText={handleChange('serviceVehicleId')}
                        onBlur={handleBlur('serviceVehicleId')}
                        value={values.serviceVehicleId}
                        label='Veículo'
                        labelStyle={formStyle().default.labelLight}
                        placeholder='Digite aqui...'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        errorMessage={errors.serviceVehicleId || null}
                        errorStyle={formStyle().default.errors}
                    />
                </View>
                <View style={formStyle().default.formRow}>
                    <Input
                        name='serviceMechanical'
                        onChangeText={handleChange('serviceMechanical')}
                        onBlur={handleBlur('serviceMechanical')}
                        value={values.serviceMechanical}
                        label='Nome do mecânico'
                        labelStyle={formStyle().default.labelLight}
                        placeholder='Digite aqui...'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        errorMessage={errors.serviceMechanical || null}
                        errorStyle={formStyle().default.errors}
                    />
                </View>
                <View style={formStyle().default.formRow}>
                    <Input
                        name='serviceDateStart'
                        onChangeText={handleChange('serviceDateStart')}
                        onBlur={handleBlur('serviceDateStart')}
                        value={values.serviceDateStart}
                        label='Data início'
                        labelStyle={formStyle().default.labelLight}
                        placeholder='Digite aqui...'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        errorMessage={errors.serviceDateStart || null}
                        errorStyle={formStyle().default.errors}
                    />
                </View>
                <View style={formStyle().default.formRow}>
                    <Input
                        name='serviceDateFinish'
                        onChangeText={handleChange('serviceDateFinish')}
                        onBlur={handleBlur('serviceDateFinish')}
                        value={values.serviceDateFinish}
                        label='Data clonclusão'
                        labelStyle={formStyle().default.labelLight}
                        placeholder='Digite aqui...'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        errorMessage={errors.serviceDateFinish || null}
                        errorStyle={formStyle().default.errors}
                    />
                </View>
                <View style={formStyle().default.formRow}>
                    <Input
                        name='serviceVehicleKm'
                        onChangeText={handleChange('serviceVehicleKm')}
                        onBlur={handleBlur('serviceVehicleKm')}
                        value={values.serviceVehicleKm}
                        label='Km atual'
                        labelStyle={formStyle().default.labelLight}
                        placeholder='Digite aqui...'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        errorMessage={errors.serviceVehicleKm || null}
                        errorStyle={formStyle().default.errors}
                    />
                </View>
                <View style={formStyle().default.formRow}>
                    <Input
                        name='serviceReplacedPieces'
                        onChangeText={handleChange('serviceReplacedPieces')}
                        onBlur={handleBlur('serviceReplacedPieces')}
                        value={values.serviceReplacedPieces}
                        label='Peças trocadas'
                        labelStyle={formStyle().default.labelLight}
                        placeholder='Digite aqui...'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        errorMessage={errors.serviceReplacedPieces || null}
                        errorStyle={formStyle().default.errors}
                    />
                </View>
                <View style={formStyle().default.formRow}>
                    <Text style={formStyle().default.labelDescLight}>Outros detalhes</Text>
                    <TextInput
                        name='serviceDescription'
                        onChangeText={handleChange('serviceDescription')}
                        onBlur={handleBlur('serviceDescription')}
                        value={values.serviceDescription}
                        placeholder='Digite aqui...'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        style={formStyle().default.textAreaDescLight}
                        multiline={true}
                        maxLength={45}
                    />
                    <Divider style={{ backgroundColor: colors().form.colorInputBorder }} />
                </View>

                <View style={formStyle().default.formRow}>
                { !btnLoading ?
                    <Button
                    onPress={handleSubmit}
                    title="Salvar"
                    buttonStyle={formStyle().default.button}
                    titleStyle={formStyle().default.buttonTitle}
                    />
                    :
                    <Button
                    buttonStyle={formStyle().default.button}
                    loading
                    />
                }
                </View>            
            </View>
        )}
        </Formik>
        <ModalAlert
            mdlIsVisible={modalIsVisible}
            mdlTitle='Cadastrado com sucesso'
            mdlFooterBtnYes='Ok'
            mdlFooterBtnYesCall={handleOnModalAlertSuccess}
        >Os dados foram salvos com sucesso!</ModalAlert>
        </>
    );
}

export default ServiceForm;
