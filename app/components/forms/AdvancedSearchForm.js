import React, {useState, useEffect} from 'react';
import {
  View
} from 'react-native';
import { Formik } from 'formik';
import { Icon, Input, Button } from 'react-native-elements';
import { DarkTheme } from './../../services/ThemeManager';

const AdvancedSearchForm = (props) => {

    const [loading, setLoading] = useState(true);
    const [btnLoading, setBtnLoading] = useState(false);
    const { navigation } = props;
    const { formStyle, colors } = DarkTheme();
    const initValues = {
        service: navigation.getParam('service'),
        company: '',
        city: '',
        state: ''
    };

    const handleFormSubmit = async (values) => {
        setBtnLoading(true);
        navigation.navigate('SearchResult', values);
    }

    useEffect(() => {
        setBtnLoading(false);
    });

    return (    
        <Formik
        initialValues={initValues}
        onSubmit={handleFormSubmit}
        >
        {({ handleChange, handleBlur, handleSubmit, values }) => (
            <View>
                <View style={formStyle().default.formRow}>
                    <Input
                        name='service'
                        onChangeText={handleChange('service')}
                        onBlur={handleBlur('service')}
                        value={values.service}
                        placeholder='Serviço automotivo'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        leftIcon={
                        <Icon
                            name='build'
                            size={24}
                            color={colors().form.colorIcon}
                            type='material'
                        />
                        }
                    />
                </View>
                <View style={formStyle().default.formRow}>
                    <Input
                        name='company'
                        onChangeText={handleChange('company')}
                        onBlur={handleBlur('company')}
                        value={values.company}
                        placeholder='Loja ou Oficina mecânica'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        leftIcon={
                        <Icon
                            name='time-to-leave'
                            size={24}
                            color={colors().form.colorIcon}
                            type='material'
                        />
                        }
                    />
                </View>
                <View style={formStyle().default.formRow}>
                    <Input
                        name='city'
                        onChangeText={handleChange('city')}
                        onBlur={handleBlur('city')}
                        value={values.city}
                        placeholder='Cidade'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        leftIcon={
                        <Icon
                            name='business'
                            size={24}
                            color={colors().form.colorIcon}
                            type='material'
                        />
                        }
                    />
                </View>
                <View style={formStyle().default.formRow}>
                    <Input
                        name='state'
                        onChangeText={handleChange('state')}
                        onBlur={handleBlur('state')}
                        value={values.state}
                        placeholder='Estado (UF)'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        leftIcon={
                        <Icon
                            name='public'
                            size={24}
                            color={colors().form.colorIcon}
                            type='material'
                        />
                        }
                    />
                </View>                
                <View style={formStyle().default.formRow}>
                { !btnLoading ?
                    <Button
                    onPress={handleSubmit}
                    title="Procurar"
                    buttonStyle={formStyle().default.button}
                    titleStyle={formStyle().default.buttonTitle}
                    />
                    :
                    <Button
                    buttonStyle={formStyle().default.button}
                    loading
                    />
                }
                </View>            
            </View>
        )}
        </Formik>
    );
}

export default AdvancedSearchForm;
