import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  TextInput,
  Text,
  StyleSheet,
} from 'react-native';
import { Formik } from 'formik';
import { Button, Divider } from 'react-native-elements';
import Modal from "react-native-modal";
import { DarkTheme } from './../../services/ThemeManager';
import ChooseImage from './../image-picker/ChooseImage';
import * as yup from 'yup';


const FormValidation = () => (
    yup.object().shape({
        description: yup
            .string()
            .required('A descrição é necessária.'),
    })
);

const  ScheduleDescriptionForm = (props) => {

    const [loading, setLoading] = useState(true);
    const [btnLoading, setBtnLoading] = useState(false);
    const [modalVisible, setModalVisible] = useState(false);
    const { navigation, onConfirm} = props;
    const { formStyle, colors } = DarkTheme();
    const initValues = {
        description: '',
        pictures: ''
    };

    const handleFormSubmit = async (values) => {
        console.log(values);
    }

    const handleConfirmAlertYes = () => {
        console.log('ok confirmado!');
        navigation.navigate('Home', {
            msg: 'schedule_confirmed_1'
        });
    }

    const handleConfirmAlertNo = () => {
        setModalVisible(false);
        console.log('cancelado!');
    }

    useEffect(() => {
        setBtnLoading(false);
        if (onConfirm) {            
            setModalVisible(true);
        }
    });

    return (
        <>
        <Formik
        initialValues={initValues}
        onSubmit={handleFormSubmit}
        validationSchema={FormValidation()}
        >
        {({ handleChange, handleBlur, handleSubmit, values, errors }) => (
            <View>                
                <View style={formStyle().default.formRow}>
                    <Text style={formStyle().default.labelDescLight}>Descrição do problema *</Text>
                    <TextInput
                        name='description'
                        onChangeText={handleChange('description')}
                        onBlur={handleBlur('description')}
                        value={values.description}
                        placeholder='Digite aqui...'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        style={formStyle().default.textAreaDescLight}
                        multiline={true}
                        maxLength={45}
                    />
                    <Text style={formStyle().default.errors}>{errors.description || null}</Text>
                </View>
                <View style={formStyle().default.formRow}>
                    <Divider style={{ backgroundColor: colors().form.colorInputBorder }} />
                </View>
                <View style={formStyle().default.formRow}>
                    <ChooseImage />
                </View>
            </View>
        )}
        </Formik>
        <Modal isVisible={modalVisible}>
            <View style={ModalStyle.container}>                
                <Text style={ModalStyle.title}>Confirmar agendamento?</Text>
                <Text style={ModalStyle.text}>Ao confirmar com "Sim",
                você precisa apenas aguardar a confirmação da outra parte,
                o prestador de serviço.
                Pode confirmar?
                </Text>
                <View style={ModalStyle.footerButtons}>
                    <Button titleStyle={[ModalStyle.footerButtonText, {color: '#666'}]} buttonStyle={[ModalStyle.footerButtonBox, {backgroundColor: '#ffffff'}]} title="Não" onPress={handleConfirmAlertNo}/>
                    <Button titleStyle={ModalStyle.footerButtonText} buttonStyle={[ModalStyle.footerButtonBox, {backgroundColor: 'green'}]} title="Sim" onPress={handleConfirmAlertYes}/>
                </View>
            </View>
        </Modal>
        </>
    );
}

const ModalStyle = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        padding: 20
    },
    title: {
        fontSize: 19,
        textAlign: "center",
        fontWeight: "bold",
        marginBottom: 15
    },
    text: {
        fontSize: 16
    },
    footerButtons: {
        flex:1,
        flexDirection: "row",
        justifyContent: 'flex-end',
        marginBottom:30,
        marginTop: 20
    },
    footerButtonBox: {
        paddingHorizontal: 35,
        paddingVertical: 20
    },
    footerButtonText: {
        fontSize: 17
    }
});

export default ScheduleDescriptionForm;
