import React, {useState, useEffect} from 'react';
import {
  View, Text
} from 'react-native';
import { Formik } from 'formik';
import { Icon, Input, Button } from 'react-native-elements';
import { DarkTheme } from './../../services/ThemeManager';
import ChooseVehicleImage from '../image-picker/ChooseVehicleImage';
import ModalAlert from '../modal/ModalAlert';
import * as yup from 'yup'

const FormValidation = () => (
    yup.object().shape({
        vehicleModel: yup
            .string()
            .required('Modelo é necessário.'),
        vehicleModelYear: yup
            .number('Digite apenas números.')
            .required('Ano do modelo é necessário.'),
        vehicleBuiltYear: yup
            .number('Digite apenas números.')
            .required('Ano de fabricação é necessário.'),
        vehicleManufacturer: yup
            .string()
            .required('A fábrica é necessária.'),
        vehicleColor: yup
            .string()
            .required('A cor é necessária.'),
        vehicleDescription: yup
            .string(),
        vehicleKm: yup
            .string()
            .required('A kilometragem é necessária.')
    })
);

const VehicleForm = (props) => {

    const [loading, setLoading] = useState(true);
    const [btnLoading, setBtnLoading] = useState(false);
    const [modalIsVisible, setModalIsVisible] = useState(false);
    const { navigation } = props;
    const { formStyle, colors } = DarkTheme();
    const initValues = {
        vehicleModelYear: '',
        vehicleBuiltYear: '',
        vehicleManufacturer: '',
        vehicleModel: '',
        vehicleColor: '',
        vehicleDescription: '',
        vehicleKm: ''
    };

    const handleFormSubmit = async (values) => {
       setBtnLoading(true);
       setModalIsVisible(true);
        console.log('salvar dados do veículo');
    }

    const handleOnModalAlertSuccess = () => {
        setModalIsVisible(false);
        navigation.navigate('Vehicles');
    }

    return (
        <>
        <Formik
        initialValues={initValues}
        onSubmit={handleFormSubmit}
        validationSchema={FormValidation()}
        >
        {({ handleChange, handleBlur, handleSubmit, values, errors }) => (
            <View>
                <View style={formStyle().default.formRow}>
                    <ChooseVehicleImage/>
                </View>
                <View style={formStyle().default.formRow}>
                    <Input
                        name='vehicleModel'
                        onChangeText={handleChange('vehicleModel')}
                        onBlur={handleBlur('vehicleModel')}
                        value={values.vehicleModel}
                        label='Modelo *'
                        labelStyle={formStyle().default.labelLight}
                        placeholder='Digite aqui...'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        errorMessage={errors.vehicleModel ? errors.vehicleModel : null}
                        errorStyle={formStyle().default.errors}
                    />
                </View>
                <View style={formStyle().default.formRow}>
                    <Input
                        name='vehicleModelYear'
                        onChangeText={handleChange('vehicleModelYear')}
                        onBlur={handleBlur('vehicleModelYear')}
                        value={values.vehicleModelYear}
                        label='Ano modelo *'
                        labelStyle={formStyle().default.labelLight}
                        placeholder='Digite aqui...'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        errorMessage={errors.vehicleModelYear ? errors.vehicleModelYear : null}
                        errorStyle={formStyle().default.errors}
                    />
                </View>
                <View style={formStyle().default.formRow}>
                    <Input
                        name='vehicleBuiltYear'
                        onChangeText={handleChange('vehicleBuiltYear')}
                        onBlur={handleBlur('vehicleBuiltYear')}
                        value={values.vehicleBuiltYear}
                        label='Ano fabricação *'
                        labelStyle={formStyle().default.labelLight}
                        placeholder='Digite aqui...'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        errorMessage={errors.vehicleBuiltYear ? errors.vehicleBuiltYear : null}
                        errorStyle={formStyle().default.errors}
                    />
                </View>
                <View style={formStyle().default.formRow}>
                    <Input
                        name='vehicleManufacturer'
                        onChangeText={handleChange('vehicleManufacturer')}
                        onBlur={handleBlur('vehicleManufacturer')}
                        value={values.vehicleManufacturer}
                        label='Fábrica * (Ex: Toyota, Honda, Ford, Fiat)'
                        labelStyle={formStyle().default.labelLight}
                        placeholder='Digite aqui...'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        errorMessage={errors.vehicleManufacturer ? errors.vehicleManufacturer : null}
                        errorStyle={formStyle().default.errors}
                    />
                </View>
                <View style={formStyle().default.formRow}>
                    <Input
                        name='vehicleColor'
                        onChangeText={handleChange('vehicleColor')}
                        onBlur={handleBlur('vehicleColor')}
                        value={values.vehicleColor}
                        label='Cor *'
                        labelStyle={formStyle().default.labelLight}
                        placeholder='Digite aqui...'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        errorMessage={errors.vehicleColor ? errors.vehicleColor : null}
                        errorStyle={formStyle().default.errors}
                    />
                </View>
                <View style={formStyle().default.formRow}>
                    <Input
                        name='vehicleKm'
                        onChangeText={handleChange('vehicleKm')}
                        onBlur={handleBlur('vehicleKm')}
                        value={values.vehicleKm}
                        label='Kilometragem *'
                        labelStyle={formStyle().default.labelLight}
                        placeholder='Digite aqui...'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        errorMessage={errors.vehicleKm ? errors.vehicleKm : null}
                        errorStyle={formStyle().default.errors}
                    />
                </View>
                <View style={formStyle().default.formRow}>
                    <Input
                        name='vehicleDescription'
                        onChangeText={handleChange('vehicleDescription')}
                        onBlur={handleBlur('vehicleDescription')}
                        value={values.vehicleDescription}
                        label='Outros detalhes. (Vísivel apenas para você)'
                        labelStyle={formStyle().default.labelLight}
                        placeholder='Digite aqui...'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        errorMessage={errors.vehicleDescription ? errors.vehicleDescription : null}
                        errorStyle={formStyle().default.errors}
                    />
                </View>
                <View style={formStyle().default.formRow}>
                { !btnLoading ?
                    <Button
                    onPress={handleSubmit}
                    title="Salvar"
                    buttonStyle={formStyle().default.button}
                    titleStyle={formStyle().default.buttonTitle}
                    />
                    :
                    <Button
                    buttonStyle={formStyle().default.button}
                    loading
                    />
                }
                </View>            
            </View>
        )}
        </Formik>
        <ModalAlert
            mdlIsVisible={modalIsVisible}
            mdlTitle='Enviado com sucesso'
            mdlFooterBtnYes='Ok'
            mdlFooterBtnYesCall={handleOnModalAlertSuccess}
        >Os dados foram salvos com sucesso!</ModalAlert>
        </>
    );
}

export default VehicleForm;
