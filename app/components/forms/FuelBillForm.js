import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput
} from 'react-native';
import { Formik } from 'formik';
import {
    Icon,
    Input,
    Button,
    Divider
} from 'react-native-elements';
import { DarkTheme } from './../../services/ThemeManager';
import ModalAlert from '../modal/ModalAlert';
import * as yup from 'yup';

const FormValidation = () => (
    yup.object().shape({
        fuelBillType: yup
            .string()
            .required('Tipo é necessário.'),
        fuelBillPayType: yup
            .string()
            .required('Tipo de pagamento é necessário.'),
        fuelBillAmount: yup
            .string()
            .required('Valor é necessário.'),
        fuelBillCompany: yup
            .string()
            .required('Nome do posto é necessário.'),
        fuelBillCompanyCity: yup
            .string(),
        fuelBillCompanyState: yup
            .string(),
        fuelBillVehicle: yup
            .number()
            .required('Veículo é necessário.'), 
        fuelBillDate: yup
            .string()
            .required('Data é necessário.')
    })
);

const FuelBillForm = (props) => {

    const [loading, setLoading] = useState(true);
    const [btnLoading, setBtnLoading] = useState(false);
    const [modalIsVisible, setModalIsVisible] = useState(false);
    const { navigation } = props;
    const { formStyle, colors } = DarkTheme();
    const initValues = {
        fuelBillType: '',
        fuelBillPayType: '',
        fuelBillAmount: '',
        fuelBillCompany: '',
        fuelBillCompanyCity: '',
        fuelBillCompanyState: '',
        fuelBillVehicle: '',   
        fuelBillDate: '',
    };

    const handleFormSubmit = async (values) => {
       setBtnLoading(true);
       setModalIsVisible(true);
        console.log('salvar dados do combustível');
    }

    const handleOnModalAlertSuccess = () => {
        setModalIsVisible(false);
        navigation.navigate('FuelBillList');
    }

    return (
        <>
        <Formik
        initialValues={initValues}
        onSubmit={handleFormSubmit}
        validationSchema={FormValidation()}
        >
        {({ handleChange, handleBlur, handleSubmit, values, errors }) => (
            <View>
                <View style={formStyle().default.formRow}>
                    <Input
                        name='fuelBillAmount'
                        onChangeText={handleChange('fuelBillAmount')}
                        onBlur={handleBlur('fuelBillAmount')}
                        value={values.fuelBillAmount}
                        label='Valor *'
                        labelStyle={formStyle().default.labelLight}
                        placeholder='Digite aqui...'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        errorMessage={errors.fuelBillAmount || null}
                        errorStyle={formStyle().default.errors}
                    />
                </View>
                <View style={formStyle().default.formRow}>
                    <Input
                        name='fuelBillType'
                        onChangeText={handleChange('fuelBillType')}
                        onBlur={handleBlur('fuelBillType')}
                        value={values.fuelBillType}
                        label='Tipo de combustível * (Ex.: Gasolina, Álcool)'
                        labelStyle={formStyle().default.labelLight}
                        placeholder='Digite aqui...'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        errorMessage={errors.fuelBillType || null}
                        errorStyle={formStyle().default.errors}
                    />
                </View>
                <View style={formStyle().default.formRow}>
                    <Input
                        name='fuelBillVehicle'
                        onChangeText={handleChange('fuelBillVehicle')}
                        onBlur={handleBlur('fuelBillVehicle')}
                        value={values.fuelBillVehicle}
                        label='Veículo *'
                        labelStyle={formStyle().default.labelLight}
                        placeholder='Digite aqui...'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        errorMessage={errors.fuelBillVehicle || null}
                        errorStyle={formStyle().default.errors}
                    />
                </View>
                <View style={formStyle().default.formRow}>
                    <Input
                        name='fuelBillDate'
                        onChangeText={handleChange('fuelBillDate')}
                        onBlur={handleBlur('fuelBillDate')}
                        value={values.fuelBillDate}
                        label='Data *'
                        labelStyle={formStyle().default.labelLight}
                        placeholder='Digite aqui...'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        errorMessage={errors.fuelBillDate || null}
                        errorStyle={formStyle().default.errors}
                    />
                </View>
                <View style={formStyle().default.formRow}>
                    <Input
                        name='fuelBillPayType'
                        onChangeText={handleChange('fuelBillPayType')}
                        onBlur={handleBlur('fuelBillPayType')}
                        value={values.fuelBillPayType}
                        label='Tipo do Pagamento (Ex.: Dinheiro, Cartão)'
                        labelStyle={formStyle().default.labelLight}
                        placeholder='Digite aqui...'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        errorMessage={errors.fuelBillPayType || null}
                        errorStyle={formStyle().default.errors}
                    />
                </View>
                <View style={formStyle().default.formRow}>
                    <Input
                        name='fuelBillCompany'
                        onChangeText={handleChange('fuelBillCompany')}
                        onBlur={handleBlur('fuelBillCompany')}
                        value={values.fuelBillCompany}
                        label='Posto *'
                        labelStyle={formStyle().default.labelLight}
                        placeholder='Digite aqui...'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        errorMessage={errors.fuelBillCompany || null}
                        errorStyle={formStyle().default.errors}
                    />
                </View>
                <View style={formStyle().default.formRow}>
                    <Input
                        name='fuelBillCompanyCity'
                        onChangeText={handleChange('fuelBillCompanyCity')}
                        onBlur={handleBlur('fuelBillCompanyCity')}
                        value={values.fuelBillCompanyCity}
                        label='Cidade Posto'
                        labelStyle={formStyle().default.labelLight}
                        placeholder='Digite aqui...'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        errorMessage={errors.fuelBillCompanyCity || null}
                        errorStyle={formStyle().default.errors}
                    />
                </View>
                <View style={formStyle().default.formRow}>
                    <Input
                        name='fuelBillCompanyState'
                        onChangeText={handleChange('fuelBillCompanyState')}
                        onBlur={handleBlur('fuelBillCompanyState')}
                        value={values.fuelBillCompanyState}
                        label='Estado Posto'
                        labelStyle={formStyle().default.labelLight}
                        placeholder='Digite aqui...'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        errorMessage={errors.fuelBillCompanyState || null}
                        errorStyle={formStyle().default.errors}
                    />
                </View>
                <View style={formStyle().default.formRow}>
                { !btnLoading ?
                    <Button
                    onPress={handleSubmit}
                    title="Salvar"
                    buttonStyle={formStyle().default.button}
                    titleStyle={formStyle().default.buttonTitle}
                    />
                    :
                    <Button
                    buttonStyle={formStyle().default.button}
                    loading
                    />
                }
                </View>            
            </View>
        )}
        </Formik>
        <ModalAlert
            mdlIsVisible={modalIsVisible}
            mdlTitle='Cadastrado com sucesso'
            mdlFooterBtnYes='Ok'
            mdlFooterBtnYesCall={handleOnModalAlertSuccess}
        >Os dados foram salvos com sucesso!</ModalAlert>
        </>
    );
}

export default FuelBillForm;
