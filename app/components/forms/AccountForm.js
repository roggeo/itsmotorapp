import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput
} from 'react-native';
import { Formik } from 'formik';
import {
    Icon,
    Input,
    Button,
    Divider
} from 'react-native-elements';
import { DarkTheme } from './../../services/ThemeManager';
import ModalAlert from '../modal/ModalAlert';
import * as yup from 'yup';

const FormValidation = () => (
    yup.object().shape({
        customerName: yup
            .string()
            .required('Nome é necessário.'),
        customerPhone: yup
            .string()
            .required('Telefone é necessário'),
        customerEmail: yup
            .string()
            .email('Insira um e-mail válido'),
        customerGender: yup
            .string(),
        customerBirth: yup
            .string(),
    })
);

const AccountForm = (props) => {

    const [loading, setLoading] = useState(true);
    const [btnLoading, setBtnLoading] = useState(false);
    const [modalIsVisible, setModalIsVisible] = useState(false);
    const { navigation } = props;
    const { formStyle, colors } = DarkTheme();
    const initValues = {
        customerName: '',
        customerPhone: '',
        customerEmail: '',
        customerGender: '',
        customerBirth: ''
    };

    const handleFormSubmit = async (values) => {
       setBtnLoading(true);
       setModalIsVisible(true);
        console.log('salvar dados do veículo');
    }

    const handleOnModalAlertSuccess = () => {
        setModalIsVisible(false);
        navigation.navigate('Home');
    }

    return (
        <>
        <Formik
        initialValues={initValues}
        onSubmit={handleFormSubmit}
        validationSchema={FormValidation()}
        >
        {({ handleChange, handleBlur, handleSubmit, values, errors }) => (
            <View>
                <View style={formStyle().default.formRow}>
                    <Input
                        name='customerName'
                        onChangeText={handleChange('customerName')}
                        onBlur={handleBlur('customerName')}
                        value={values.customerName}
                        label='Nome *'
                        labelStyle={formStyle().default.labelLight}
                        placeholder='Digite aqui...'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        errorMessage={errors.customerName || null}
                        errorStyle={formStyle().default.errors}
                    />
                </View>                
                <View style={formStyle().default.formRow}>
                    <Input
                        name='customerPhone'
                        onChangeText={handleChange('customerPhone')}
                        onBlur={handleBlur('customerPhone')}
                        value={values.customerPhone}
                        label='Telefone *'
                        labelStyle={formStyle().default.labelLight}
                        placeholder='Digite aqui...'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        errorMessage={errors.customerPhone || null}
                        errorStyle={formStyle().default.errors}
                    />
                </View>
                <View style={formStyle().default.formRow}>
                    <Input
                        name='customerEmail'
                        onChangeText={handleChange('customerEmail')}
                        onBlur={handleBlur('customerEmail')}
                        value={values.customerEmail}
                        label='E-mail'
                        labelStyle={formStyle().default.labelLight}
                        placeholder='Digite aqui...'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        errorMessage={errors.customerEmail || null}
                        errorStyle={formStyle().default.errors}
                    />
                </View>
                <View style={formStyle().default.formRow}>
                    <Input
                        name='customerGender'
                        onChangeText={handleChange('customerGender')}
                        onBlur={handleBlur('customerGender')}
                        value={values.customerGender}
                        label='Gênero (Masculino, Feminino ...)'
                        labelStyle={formStyle().default.labelLight}
                        placeholder='Digite aqui...'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        errorMessage={errors.customerGender || null}
                        errorStyle={formStyle().default.errors}
                    />
                </View>
                <View style={formStyle().default.formRow}>
                    <Input
                        name='customerBirth'
                        onChangeText={handleChange('customerBirth')}
                        onBlur={handleBlur('customerBirth')}
                        value={values.customerBirth}
                        label='Data nascimento'
                        labelStyle={formStyle().default.labelLight}
                        placeholder='Digite aqui...'
                        placeholderTextColor={colors().form.colorPlaceholder}
                        inputStyle={formStyle().default.inputLight}
                        inputContainerStyle={formStyle().default.inputContainerStyle}
                        containerStyle={formStyle().default.inputContainer}
                        errorMessage={errors.customerBirth || null}
                        errorStyle={formStyle().default.errors}
                    />
                </View>
                <View style={formStyle().default.formRow}>
                { !btnLoading ?
                    <Button
                    onPress={handleSubmit}
                    title="Salvar"
                    buttonStyle={formStyle().default.button}
                    titleStyle={formStyle().default.buttonTitle}
                    />
                    :
                    <Button
                    buttonStyle={formStyle().default.button}
                    loading
                    />
                }
                </View>            
            </View>
        )}
        </Formik>
        <ModalAlert
            mdlIsVisible={modalIsVisible}
            mdlTitle='Cadastrado com sucesso'
            mdlFooterBtnYes='Ok'
            mdlFooterBtnYesCall={handleOnModalAlertSuccess}
        >Os dados foram salvos com sucesso!</ModalAlert>
        </>
    );
}

export default AccountForm;
