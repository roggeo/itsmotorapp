import React, {useState, useEffect} from 'react';
import {
  View
} from 'react-native';
import { Formik } from 'formik';
import { Icon, Input, Button } from 'react-native-elements';
import { DarkTheme } from './../../services/ThemeManager';

const SearchForm = (props) => {

  const [btnLoading, setBtnLoading] = useState(false);
  const { navigation } = props;
  const { formStyle, colors } = DarkTheme();
  const initValues = {
    service: ''
  };

  const handleFormSubmit = async (values) => {
    setBtnLoading(true);
    setTimeout(() => {
      navigation.navigate('SearchResult', values);
    }, 200); 
  }

  useEffect(() => {
    setTimeout(() => {
      setBtnLoading(false);
    }, 200); 
  });

  return (    
    <Formik
      initialValues={initValues}
      onSubmit={handleFormSubmit}
    >
      {({ handleChange, handleBlur, handleSubmit, values }) => (
        <View>
            <View style={formStyle().default.formRow}>
              <Input
                name='service'
                onChangeText={handleChange('service')}
                onBlur={handleBlur('service')}
                value={values.service}
                placeholder='Qual tipo de serviço automotivo?'
                placeholderTextColor={colors().form.colorPlaceholder}
                inputStyle={formStyle().default.input}
                inputContainerStyle={formStyle().default.inputContainerStyle}
                containerStyle={formStyle().default.inputContainer}
                leftIcon={
                  <Icon
                    name='build'
                    size={24}
                    color={colors().form.colorIcon}
                    type='material'
                  />
                }
              />
            </View>
            <View style={formStyle().default.formRow}>
              { !btnLoading ?
                <Button
                  onPress={handleSubmit}
                  title="Procurar"
                  buttonStyle={formStyle().default.button}
                  titleStyle={formStyle().default.buttonTitle}
                  />
                :
                <Button
                  buttonStyle={formStyle().default.button}
                  loading
                  />
              }
            </View>            
        </View>
      )}
    </Formik>
  );
  }

export default SearchForm;
