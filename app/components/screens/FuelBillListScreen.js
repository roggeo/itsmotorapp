import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  Text
} from 'react-native';
import {
  ListItem
} from 'react-native-elements';
import ListStyle from './styles/ListStyle';
import { TouchableOpacity } from 'react-native-gesture-handler';
import {getDateBrFormat} from './../../utils/DateUtils';

const list = [
  {
    fuelBillId: 255,
    fuelBillType: 'Gasolina e Alcool',
    fuelBillCurrency: 'R$',
    fuelBillPayType: 'Dinheiro',
    fuelBillAmount: 60,
    fuelBillCompany: 'Ipiranga Centro',
    fuelBillCompanyCity: 'Anapolis',
    fuelBillCompanyState: 'Goías',
    fuelBillVehicle: 'HONDA civic 2.0 Lx',    
    fuelBillDate: '2020-06-03',
  },
  {
    fuelBillId: 308,
    fuelBillType: 'Alcool',
    fuelBillCurrency: 'R$',
    fuelBillPayType: 'Cartão',
    fuelBillAmount: 82.35,
    fuelBillCompany: 'Shell Centro',
    fuelBillCompanyCity: 'Anapolis',
    fuelBillCompanyState: 'Goías',
    fuelBillVehicle: 'HONDA civic 2.0 Lx',    
    fuelBillDate: '2020-07-15',
  },
];

const FuelBillListScreen = (props) => {

  const { navigation } = props;
  
  const handlePress = (id) => {
    navigation.navigate('FuelBill', {
      id: id
    });
  }

  const getTitle = (data) => (
    `${data.fuelBillType} (${data.fuelBillCurrency} ${data.fuelBillAmount} - ${data.fuelBillPayType})`
  );

  const getSubTitle = (data) => (
    `${getDateBrFormat(data.fuelBillDate)} - ${data.fuelBillVehicle} - Posto ${data.fuelBillCompany}`
  );

  return <SafeAreaView>
    <ScrollView contentInsetAdjustmentBehavior="always">
      {
      list.map((l, i) => (
        <ListItem
          key={i}
          title={
            <TouchableOpacity onPress={handlePress.bind(this, l.fuelBillId)}>
              <Text  style={ListStyle.titleUpcase}>{getTitle(l)}</Text>
            </TouchableOpacity>
          }
          subtitle={
            <TouchableOpacity onPress={handlePress.bind(this, l.fuelBillId)}>
              <Text style={ListStyle.subtitleUpcase}>{getSubTitle(l)}</Text>
            </TouchableOpacity>
          }
          bottomDivider
        />
      ))
    }
    </ScrollView>
  </SafeAreaView>;
}

export default FuelBillListScreen;