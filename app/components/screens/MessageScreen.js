import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  SafeAreaView,
  ScrollView,
  StyleSheet
} from 'react-native';

const MessageScreen = (props) => {

  const { navigation } = props;

  const [user, setUser] = useState({
    name: navigation.getParam('name'),
    id:  navigation.getParam('userId')
  });
  
  return (
    <SafeAreaView style={Style.containerSafeArea}>
      <ScrollView contentInsetAdjustmentBehavior="always">
        <Text>Mensagens de {user.name} (#{user.id}) </Text>
      </ScrollView>
    </SafeAreaView>
  );
}

const Style = StyleSheet.create({
  containerSafeArea: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
  }
});

export default MessageScreen;