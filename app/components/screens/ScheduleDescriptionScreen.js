import React, { useEffect, useState } from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text
} from 'react-native';
import { Button, Icon } from 'react-native-elements';
import ScheduleDescriptionForm from './../forms/ScheduleDescriptionForm';
import Style from './styles/ScheduleDescriptionStyle';

const ScheduleDescriptionScreen = (props) => {

    const [loading, setLoading] = useState(true);
    const [isConfirm, setIsConfirm] = useState(false);
    const { navigation } = props;

    const handleConfirm = () => {
        setIsConfirm(true);
    }

    useEffect(() => {
        setLoading(false);
        setIsConfirm(false);
    });

    if (loading) {
        return (
            <View style={Style.containerLoading}>
               <Text>Carregando...</Text>
            </View>
        );
    } else {
        return (
            <SafeAreaView style={Style.containerSafeArea}>
                <ScrollView contentInsetAdjustmentBehavior="always">
                    <View>
                        <ScheduleDescriptionForm onConfirm={isConfirm} navigation={navigation}/>
                    </View>
                </ScrollView>
                <View style={Style.bottomContainer}>
                <Button
                    onPress={handleConfirm}
                    buttonStyle={Style.bottomBtn}
                    titleStyle={Style.bottomBtnTitle}
                    icon={
                    <Icon
                        name="alarm-on"
                        color="#27a566"
                        type="material"
                    />
                    }          
                    title="Agendar"
                    iconLeft
                />
            </View>
            </SafeAreaView>
        );
    }
}

export default ScheduleDescriptionScreen;