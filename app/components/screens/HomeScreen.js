import React, { useEffect } from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  StatusBar,
  Alert,
} from 'react-native';

import SearchForm from '../forms/SearchForm';
import GoogleMaps from '../google-maps/GoogleMaps';
import { DarkTheme } from '../../services/ThemeManager';
import { getMessageByHttp } from './../../services/MessagesByHttp';

const HomeScreen: () => React$Node = (props) => {

  const { appStyle, colors } = DarkTheme();
  const { navigation } = props;

  const showAlertMessage = () => {
    new Promise((resolve) => {
      let msg = navigation.getParam('msg');
      resolve(msg);
    }).then((msg) => {
      if (msg) {
        const message = getMessageByHttp(msg);
        Alert.alert(message[0], message[1]);
      }
    }).catch((err) => {
      throw  err;
    });
  }

  useEffect(() => {
    showAlertMessage();
  });
  
  return (
    <>
      <StatusBar backgroundColor={colors().colorTopBarBg} barStyle="light-content"/>
      <SafeAreaView style={appStyle().containerSafeArea}>
        <ScrollView contentInsetAdjustmentBehavior="automatic">
          <View style={appStyle().body}>
            <GoogleMaps/>
            <SearchForm navigation={navigation} />
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default HomeScreen;
