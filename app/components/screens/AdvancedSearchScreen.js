import React, { useEffect, useState } from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  StyleSheet
} from 'react-native';
import AdvancedSearchForm from './../forms/AdvancedSearchForm';

const AdvancedSearchScreen = (props) => {

    const [loading, setLoading] = useState(true);
    const { navigation } = props;

    useEffect(() => {
        setLoading(false);
    });

    if (loading) {
        return (
            <View style={Style.containerLoading}>
               <Text>Carregando...</Text>
            </View>
        );
    } else {
        return (
            <SafeAreaView style={Style.containerSafeArea}>
                <ScrollView contentInsetAdjustmentBehavior="always">
                    <View>
                        <AdvancedSearchForm navigation={navigation}/>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }    
}

const Style = StyleSheet.create({
    containerSafeArea: {
        flex: 1
    },
    containerLoading: {
        flex: 1,
        //justifyContent: 'center',
        alignItems: 'center'
    }
});

export default AdvancedSearchScreen;