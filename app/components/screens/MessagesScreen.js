import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  Text
} from 'react-native';
import {
  ListItem
} from 'react-native-elements';
import Style from './styles/ListStyle';
import { TouchableOpacity } from 'react-native-gesture-handler';

const list = [
  {
    userId: 5,
    name: 'Amy Farha',
    avatarUrl: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: 'O carro está com novo problema',
    notRead: 2
  },
  {
    userId: 8,
    name: 'Chris Jackson',
    avatarUrl: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Estamos quase terminando o serviço blz?',
    notRead: 5
  },
  {
    userId: 12,
    name: 'Eva Jackson',
    avatarUrl: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: 'Estamos quase terminando o serviço blz?',
    notRead: 1
  }
];

const MessagesScreen = (props) => {

  const { navigation } = props;
  
  const handlePress = (name, userId) => {
    navigation.navigate('Message', {
      name: name,
      userId: userId
    });
  }

  return <SafeAreaView>
    <ScrollView contentInsetAdjustmentBehavior="always">
      {
      list.map((l, i) => (
        <ListItem
          key={i}
          leftAvatar={{rounded: true, source: { uri: l.avatarUrl } }}
          title={
            <TouchableOpacity onPress={handlePress.bind(this, l.name, l.userId)}>
              <Text style={Style.titleUpcase} onPress={handlePress.bind(this, l.name, l.userId)}>{l.name}</Text>            
            </TouchableOpacity>
          }
          subtitle={            
            <TouchableOpacity onPress={handlePress.bind(this, l.name, l.userId)}>
              <Text style={Style.subtitle}>{l.subtitle}</Text>            
            </TouchableOpacity>
          }
          bottomDivider
          badge={{
            value: l.notRead,
            status: "success",
            containerStyle: Style.badgeContainerStyle
          }}
        />
      ))
    }
    </ScrollView>
  </SafeAreaView>;
}

export default MessagesScreen;