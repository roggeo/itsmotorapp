import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  Text
} from 'react-native';
import {
  ListItem
} from 'react-native-elements';
import ListStyle from './styles/ListStyle';
import { TouchableOpacity } from 'react-native-gesture-handler';
import {getDateBrFormat} from './../../utils/DateUtils';

const list = [
  {
    serviceId: 255,
    serviceName: 'Troca de óleo',
    serviceVehicle: 'HONDA civic 2.0 Lx',
    serviceMechanical: 'João Ricardo',
    serviceDateStart: '2020-05-03',
    serviceDateFinish: '2020-05-09',
  },
  {
    serviceId: 305,
    serviceName: 'Troca de óleo',
    serviceVehicle: 'HONDA civic 2.0 Lx',
    serviceMechanical: 'Frederico',
    serviceDateStart: '2020-04-07',
    serviceDateFinish: '2020-06-01',
  },
];

const ServicesScreen = (props) => {

  const { navigation } = props;
  
  const handlePress = (id) => {
    navigation.navigate('Service', {
      id: id
    });
  }

  const getServiceTitle = (data) => (
    `S${data.serviceId} - ${data.serviceVehicle}`
  );

  const getServiceSubTitle = (data) => (
    `S${data.serviceName} - ${getDateBrFormat(data.serviceDateStart)}`
  );

  return <SafeAreaView>
    <ScrollView contentInsetAdjustmentBehavior="always">
      {
      list.map((l, i) => (
        <ListItem
          key={i}
          title={
            <TouchableOpacity onPress={handlePress.bind(this, l.serviceId)}>
              <Text  style={ListStyle.titleUpcase}>{getServiceTitle(l)}</Text>
            </TouchableOpacity>
          }
          subtitle={
            <TouchableOpacity onPress={handlePress.bind(this, l.serviceId)}>
              <Text style={ListStyle.subtitleUpcase}>{getServiceSubTitle(l)}</Text>
            </TouchableOpacity>
          }
          bottomDivider
        />
      ))
    }
    </ScrollView>
  </SafeAreaView>;
}

export default ServicesScreen;