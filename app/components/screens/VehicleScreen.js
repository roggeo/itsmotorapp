import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  SafeAreaView,
  ScrollView,
  StyleSheet
} from 'react-native';
import VehicleForm from './../forms/VehicleForm';

const VehicleScreen = (props) => {

  const { navigation } = props;
  const [loading, setLoading] = useState(true);

  const [vehicle, setVehicle] = useState({
    id:  navigation.getParam('id')
  });

  useEffect(() => {
      setLoading(false);
  });

  if (loading) {
      return (
          <View style={Style.containerLoading}>
             <Text>Carregando...</Text>
          </View>
      );
  } else {
      return (
        <SafeAreaView style={Style.containerSafeArea}>
            <ScrollView contentInsetAdjustmentBehavior="always">
              <VehicleForm navigation={navigation}/>
            </ScrollView>
        </SafeAreaView>
      );
  }    
}

const Style = StyleSheet.create({
  containerSafeArea: {
      flex: 1
  },
  containerLoading: {
    flex: 1,
    alignItems: 'center'
}
});

export default VehicleScreen;