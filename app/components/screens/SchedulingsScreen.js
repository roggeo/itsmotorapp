import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  Text
} from 'react-native';
import {
  ListItem
} from 'react-native-elements';
import ListStyle from './styles/ListStyle';
import { TouchableOpacity } from 'react-native-gesture-handler';
import {getDateBrFormat} from '../../utils/DateUtils';

const list = [
  {
    schedulingId: '126',
    schedulingAccepted: true,
    schedulingDescription: 'Agendamento criado pelo tela inicial do aplicativo',
    schedulingRequestDate: '2020-06-02',
    schedulingAcceptedDate: '2020-06-03',
    schedulingVehicle: 'Toyota Corolla 1.8'
  },
  {
    schedulingId: '188',
    schedulingAccepted: true,
    schedulingDescription: 'Tela inicial do aplicativo',
    schedulingRequestDate: '2020-05-21',
    schedulingAcceptedDate: '2020-05-22',    
    schedulingVehicle: 'Honda Civic 2.0'
  },
  {
    schedulingId: '195',
    schedulingAccepted: false,
    schedulingDescription: 'Outro agendamento do aplicativo',
    schedulingRequestDate: '2020-11-02',
    schedulingAcceptedDate: null,
    schedulingVehicle: 'Volkswagen GOL GTX L3'
  },
];

const SchedulingsScreen = (props) => {

  const { navigation } = props;
  
  const handlePress = (id) => {
    navigation.navigate('Scheduling', {
      id: id
    });
  }

  const getSchedulingTitle = (data) => (
    `${data.schedulingVehicle}`
  );

  const getSchedulingSubTitle = (data) => (
    `${data.schedulingRequestDate} - Pedido ${getScheduleType(data.schedulingAcceptedDate)}`
  );

  const getScheduleType = (dt) => (
    (!dt) ? 'pendente' : `aceito dia ${getDateBrFormat(dt)}`
  )

  return <SafeAreaView>
    <ScrollView contentInsetAdjustmentBehavior="always">
      {
      list.map((l, i) => (
        <ListItem
          key={i}
          title={
            <TouchableOpacity onPress={handlePress.bind(this, l.schedulingId)}>
              <Text  style={ListStyle.titleUpcase}>{getSchedulingTitle(l)}</Text>
            </TouchableOpacity>
          }
          subtitle={
            <TouchableOpacity onPress={handlePress.bind(this, l.schedulingId)}>
              <Text style={ListStyle.subtitleUpcase}>{getSchedulingSubTitle(l)}</Text>
            </TouchableOpacity>
          }
          bottomDivider
        />
      ))
    }
    </ScrollView>
  </SafeAreaView>;
}

export default SchedulingsScreen;