import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  Text,
  View,
  StyleSheet,
  TouchableOpacity
} from 'react-native';
import {
  Image
} from 'react-native-elements';
import { AssetsImages } from './../../services/AssetsManager';
import Style from './styles/ScheduleVehicleStyle';


const searchResult = [
  {
    vehicleModel: 'GOL GTX L3',
    vehicleManufacturer: 'Volkswagen',
    vehicleColor: 'Cinza',
    vehicleLicensePlate: 'JGO6072',
    vehicleBuiltYear: 2009,
  },
  {
    vehicleModel: 'FOX',
    vehicleManufacturer: 'Volkswagen',
    vehicleColor: 'Preto',
    vehicleLicensePlate: 'PKQ8072',
    vehicleBuiltYear: 2015,
  },
];

const { noImageVehicle } = AssetsImages();

const ScheduleVehicleScreen = (props) => {

  const { navigation } = props;

  const handleSkipToDescription = () => {
    navigation.navigate('ScheduleDescription', {
      service: navigation.getParam('service')
    });
  }

  return (
      <SafeAreaView style={Style.containerSafeArea}>
        <ScrollView
          contentInsetAdjustmentBehavior="always"
        >
          {searchResult.map((val, index) =>
            <View key={index}  style={{backgroundColor: (index % 2 ? '#effff6' : '#def5ef')}}>
              <View style={Style.resultRow}>              
                <Image style={Style.resultRowImg} source={noImageVehicle()}/>
                <View style={{width:'55%'}}>
                  <Text style={Style.resultRowTitle}>
                  {val.vehicleManufacturer} {val.vehicleModel}
                  </Text>
                  <Text style={Style.resultRowDesc}>{val.vehicleColor} - {val.vehicleBuiltYear} - {val.vehicleLicensePlate}</Text>
                </View>                
                <TouchableOpacity onPress={handleSkipToDescription} style={Style.resultRowBtn}>    
                  <Text style={{color: 'white'}}>Selecionar</Text>
                </TouchableOpacity>
              </View>
            </View>
          )}
        </ScrollView>
      </SafeAreaView>
    );
}

export default ScheduleVehicleScreen;