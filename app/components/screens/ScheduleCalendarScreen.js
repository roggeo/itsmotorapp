import React, { useEffect, useState } from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text
} from 'react-native';
import { Button, Icon } from 'react-native-elements';
import Style from './styles/ScheduleCalendarStyle';
import SchuduleCalendar from './../calendar/ScheduleCalendar';
import { arrayChunk } from './../../utils/Array';

const ScheduleCalendarScreen = (props) => {

    const [loading, setLoading] = useState(true);
    const { navigation } = props;

    useEffect(() => {
        setLoading(false);
    });

    const handleSkipToVehicle = () => {
        navigation.navigate('ScheduleVehicle', {
            service: navigation.getParam('service')
        });
    }

    if (loading) {
        return (
            <View style={Style.containerLoading}>
               <Text>Carregando...</Text>
            </View>
        );
    } else {
        return <SafeAreaView style={Style.containerSafeArea}>
            <ScrollView contentInsetAdjustmentBehavior="always">
                <SchuduleCalendar/>
            </ScrollView>
            <View style={Style.bottomContainer}>
                <Button
                    onPress={handleSkipToVehicle}
                    buttonStyle={Style.bottomBtn}
                    titleStyle={Style.bottomBtnTitle}
                    icon={
                    <Icon
                        name="arrow-forward"
                        color="#27a566"
                        type="material"
                    />
                    }          
                    title="Avançar"
                    iconRight
                />
            </View>
        </SafeAreaView>
    }
}

export default ScheduleCalendarScreen;