import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  SafeAreaView,
  ScrollView,
  StyleSheet
} from 'react-native';

const SchedulingScreen = (props) => {

  const { navigation } = props;

  const [scheduling, setScheduling] = useState({
    id:  navigation.getParam('id')
  });
  
  return (
    <SafeAreaView style={Style.containerSafeArea}>
      <ScrollView contentInsetAdjustmentBehavior="always">
        <Text>Detalhes do Agendamento #{scheduling.id}</Text>
      </ScrollView>
    </SafeAreaView>
  );
}

const Style = StyleSheet.create({
  containerSafeArea: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
  }
});

export default SchedulingScreen;