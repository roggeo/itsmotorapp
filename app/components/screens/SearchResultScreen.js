import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  Text,
  View,
  TouchableOpacity
} from 'react-native';
import {
  Button,
  Icon,
  Image
} from 'react-native-elements';
import { AssetsImages } from './../../services/AssetsManager';
import Style from './styles/SearchResultStyle';


const searchResult = [
  {
    companyName: 'Serviços Automotivos da Bmw',
    companyNickname: 'Bmw Auto',
    companyLat:-16.3286956,
    companyLon:-48.9495158,
    companyCity: 'Anápolis',
    companyState: 'GO'
  },
  {
    companyName: 'Brasil Estética Automotiva',
    companyNickname: 'Auto Estética',
    companyLat:-16.3324572,
    companyLon:-48.9527363,
    companyCity: 'Anápolis',
    companyState: 'GO'
  },
  {
    companyName: 'Ana Som Automotivo',
    companyNickname: 'Anasom Aqui Mais que você',
    companyLat:-16.3246116,
    companyLon:-48.9528209,
    companyCity: 'Anápolis',
    companyState: 'GO'
  },
  {
    companyName: 'BBBFFF Ana Som Automotivo',
    companyNickname: 'NMMM Anasom',
    companyLat:-16.3246116,
    companyLon:-48.9528209,
    companyCity: 'Anápolis',
    companyState: 'GO'
  },
  {
    companyName: 'Ana Som Automotivo',
    companyNickname: 'Anasom',
    companyLat:-16.3246116,
    companyLon:-48.9528209,
    companyCity: 'Anápolis',
    companyState: 'GO'
  }
];

const { noImageCompany } = AssetsImages();

const SearchResultScreen = (props) => {

  const { navigation } = props;

  const handleAdvancedSerach = async () => {
    navigation.navigate('AdvancedSearch', {
      service: navigation.getParam('service')
    });
  }

  const handleSkipToSchedule = () => {
    navigation.navigate('ScheduleCalendar', {
      service: navigation.getParam('service')
    });
  }

  return (
      <SafeAreaView style={Style.containerSafeArea}>
        <ScrollView
          contentInsetAdjustmentBehavior="always"
        >
          <View style={Style.topScreen}>
            <Button
              onPress={handleAdvancedSerach}
              buttonStyle={Style.topScreenBtn}
              titleStyle={Style.topScreenBtnTitle}
              icon={
                <Icon
                  name="search"
                  color="#3d3d3d"
                  type="material"
                />
              }          
              title="Pesquisa avançada"
              iconLeft         
            />
          </View>
          {searchResult.map((val, index) =>
            <View key={index}  style={{backgroundColor: (index % 2 ? '#effff6' : '#def5ef')}}>
              <View style={Style.resultRow}>              
                <Image style={Style.resultRowImg} source={noImageCompany()}/>
                <View style={{width:'58%'}}>
                  <Text style={Style.resultRowTitle}>
                  {val.companyNickname}
                  </Text>
                  <Text style={Style.resultRowDesc}>{val.companyCity} - {val.companyState}</Text>
                </View>                
                <TouchableOpacity onPress={handleSkipToSchedule} style={Style.resultRowBtn}>    
                  <Text style={{color: 'white'}}>Agendar</Text>
                </TouchableOpacity>
              </View>
            </View>
          )}
        </ScrollView>
        <View style={Style.bottomContainer}>
          <Button
            buttonStyle={Style.bottomBtn}
            titleStyle={Style.bottomBtnTitle}
            icon={
              <Icon
                name="arrow-downward"
                color="#27a566"
                type="material"
              />
            }          
            title="Ver mais"
            iconRight
          />
        </View>
      </SafeAreaView>
    );
}

export default SearchResultScreen;