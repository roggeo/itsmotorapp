import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  Text
} from 'react-native';
import {
  ListItem
} from 'react-native-elements';
import ListStyle from './styles/ListStyle';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { AssetsImages } from './../../services/AssetsManager';


const list = [
  {
    vehicleId: 8,
    vehicleModel: 'GOL GTX L3',
    vehicleManufacturer: 'Volkswagen',
    vehicleColor: 'Cinza',
    vehicleLicensePlate: 'JGO6072',
    vehicleModelYear: 2009,
    vehicleImageUrl: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
  },
  {
    vehicleId: 12,
    vehicleModel: 'HONDA civic 2.0 Lx',
    vehicleManufacturer: 'Honda',
    vehicleColor: 'Preta',
    vehicleLicensePlate: 'JBB6072',
    vehicleModelYear: 2013,
    vehicleImageUrl:  'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
  }
];

const { noImageVehicle } = AssetsImages();

const VehiclesScreen = (props) => {

  const { navigation } = props;
  
  const handlePress = (title, id) => {
    navigation.navigate('Vehicle', {
      title: title,
      id: id
    });
  }

  const getVehicleTitle = (data) => (
    `${data.vehicleModel}`
  );

  const getVehicleSubTitle = (data) => (
    `${data.vehicleColor} - ${data.vehicleLicensePlate} - ${data.vehicleModelYear}`
  );

//leftAvatar={{rounded: true, source: { uri: l.vehicleImageUrl } }}

  return <SafeAreaView>
    <ScrollView contentInsetAdjustmentBehavior="always">
      {
      list.map((l, i) => (
        <ListItem
          key={i}
          leftAvatar={{rounded: true, source: noImageVehicle() }}
          title={
            <TouchableOpacity onPress={handlePress.bind(this, getVehicleTitle(l), l.vehicleId)}>
              <Text  style={ListStyle.titleUpcase}>{`${l.vehicleId}`} - {getVehicleTitle(l)}</Text>
            </TouchableOpacity>
          }
          subtitle={
            <TouchableOpacity onPress={handlePress.bind(this, getVehicleTitle(l), l.vehicleId)}>
              <Text style={ListStyle.subtitleUpcase}>{getVehicleSubTitle(l)}</Text>
            </TouchableOpacity>
          }
          bottomDivider
        />
      ))
    }
    </ScrollView>
  </SafeAreaView>;
}

export default VehiclesScreen;