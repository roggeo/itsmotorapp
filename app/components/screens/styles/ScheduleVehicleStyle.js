import { StyleSheet } from 'react-native';

const Style = StyleSheet.create({
    containerSafeArea: {
        flex: 1
    },
    resultRow: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        paddingVertical: 8,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    resultRowDesc: {
        color: '#555555'
    },
    resultRowImg: {
        width: 60,
        height: 60,
        marginRight: 15
    },
    resultRowTitle: {
        textTransform: 'uppercase',
        color: '#000000',
        fontWeight: 'bold'
    },
    resultRowBtn: {
        alignSelf: 'stretch',
        width: '24%',
        backgroundColor: '#27a566',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5
    }
});

export default Style;