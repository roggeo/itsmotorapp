import { StyleSheet } from 'react-native';

const Style = StyleSheet.create({
    containerSafeArea: {
        flex: 1
    },
    topScreen: {
        marginHorizontal: 15,
        marginVertical: 15
    },
    topScreenBtn: {
        backgroundColor: '#ffffff',
    },
    topScreenBtnTitle: {
        color: '#3d3d3d',
        textTransform: 'uppercase'
    },
    resultRow: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        paddingVertical: 8,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    resultRowDesc: {
        color: '#555555'
    },
    resultRowImg: {
        width: 60,
        height: 60,
        marginRight: 15
    },
    resultRowTitle: {
        textTransform: 'uppercase',
        color: '#000000',
        fontWeight: 'bold'
    },
    resultRowBtn: {
        alignSelf: 'stretch',
        width: '20%',
        backgroundColor: '#27a566',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5
    },
    bottomContainer: {
        backgroundColor: '#ffffff',
        borderTopWidth: 1,
        borderTopColor: '#27a566'
    },
    bottomBtn: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: '#ffffff',
    },
    bottomBtnTitle: {
        color: '#27a566',
        textTransform: 'uppercase',
    }
});

export default Style;