import { StyleSheet } from 'react-native';

const Style = StyleSheet.create({
    badgeContainerStyle: {
        marginTop: -20
    },
    title: {
        color: '#222222',
        textTransform: 'capitalize'
    },
    subtitle: {
        color: '#888888',
    },
    titleUpcase: {
        color: '#222222',
        textTransform: 'uppercase'
    },
    subtitleUpcase: {
        color: '#888888',
        textTransform: 'uppercase'
    }
});

export default Style;