import { StyleSheet } from "react-native";

const Style = StyleSheet.create({
    containerSafeArea: {
        flex: 1
    },
    containerLoading: {
        flex: 1,
        //justifyContent: 'center',
        alignItems: 'center'
    },
    bottomContainer: {
        backgroundColor: '#ffffff',
        borderTopWidth: 1,
        borderTopColor: '#27a566'
    },
    bottomBtn: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: '#ffffff',
    },
    bottomBtnTitle: {
        color: '#27a566',
        textTransform: 'uppercase',
    }
});

export default Style;