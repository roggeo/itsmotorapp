import React from 'react';
import {
  View,
  TouchableOpacity,
  Image
} from 'react-native';

import { DarkTheme }  from './../../services/ThemeManager';
import { AssetsImages } from './../../services/AssetsManager';

const { headerStyle } = DarkTheme();
const { logo } = AssetsImages();

const Header = (props) => {
  <View style={headerStyle().headerView}>
      <Image
          style={headerStyle().logo}
          source={logo()}
      />
  </View>
}

export default Header;
