import React, {useState} from 'react';
import ImagePicker from 'react-native-image-picker';
import { Image, Button, Icon } from 'react-native-elements';
import {
  StyleSheet,
  View,
  Text,
  Dimensions
} from 'react-native';
import { AlertInfo } from './../../helpers/AlertMessage';
import { arrayChunk } from '../../utils/Array';

const options = {
  title: 'Selecione a foto',
  //customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

const ChooseVehicleImage = ()=> {

    const [picture, setPicture] = useState();

    const handleOnPress = () => {
        ImagePicker.showImagePicker(options, (response) => {

        if (response.didCancel) {
            console.log('User cancelled image picker');
        } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
        } else {
            // const source = { uri: 'data:image/jpeg;base64,' + response.data };    
            const source = { uri: response.uri };
            setPicture(source);
        }
        });
    }

    const ImageVehicle = () => (
        <View style={Style.containerPictures}>
            <View style={Style.carPicture}>
                <Image source={picture} style={Style.picture} />
            </View>
        </View>
    );

    return (<>
        <Button
            buttonStyle={Style.button}
            titleStyle={Style.buttonTitle}
            onPress={handleOnPress}
            title="Adicionar foto"
            iconLeft
            icon={
            <Icon
                name="photo-camera"
                color="#3d3d3d"
                type="material"
            />
            }
        /> 
        {picture ? ImageVehicle() : null}      
  </>);
}

const widthForPicture = (Dimensions.get('window').width / 2);

const Style = StyleSheet.create({
  containerPictures: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 10,
    backgroundColor: '#e1e1e1'
  },
  carPicture: {
    marginHorizontal: 5,
    marginVertical: 5,    
  },
  picture: {
    width: widthForPicture - 16,
    height: widthForPicture,
  },
  button: {
    backgroundColor: '#ffffff',
  },
  buttonTitle: {
      color: '#3d3d3d',
      textTransform: 'uppercase'
  }
});

export default ChooseVehicleImage;


