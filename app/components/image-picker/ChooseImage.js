import React, {useState} from 'react';
import ImagePicker from 'react-native-image-picker';
import { Image, Button, Icon } from 'react-native-elements';
import {
  StyleSheet,
  View,
  Text,
  Dimensions
} from 'react-native';
import { AlertInfo } from './../../helpers/AlertMessage';
import { arrayChunk } from '../../utils/Array';

const options = {
  title: 'Selecione a foto',
  //customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

const ChooseImage = ()=> {

  const [pictures, setPictures] = useState([]);

  const handleOnPress = () => {
    ImagePicker.showImagePicker(options, (response) => {

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };    
        const source = { uri: response.uri };        
        let _pictures = pictures.flat(1).concat(source);
        _pictures = arrayChunk(_pictures, 3);
        setPictures(_pictures);
      }
    });
  }

  const alertInfo = () => (<AlertInfo>
    Adicione fotos para documentar o problema do seu veículo.
    Isso também vai ajudar a pessoa que vai confirmar o agendamento do serviço.
    Pressione o botão a cima "Adicionar Foto".
  </AlertInfo>);

  return (<>
      <Button
        buttonStyle={Style.button}
        titleStyle={Style.buttonTitle}
        onPress={handleOnPress}
        title="Adicionar foto"
        iconLeft
        icon={
          <Icon
            name="photo-camera"
            color="#3d3d3d"
            type="material"
          />
        }
      /> 

      <View style={Style.containerPictures}>
        {pictures.map((picGroup, indexGroup) => 
          <View key={indexGroup} style={Style.rowPictures}>
            {picGroup.map((val, index) =>
                <View key={index} style={Style.carPicture}>
                  <Image source={val} style={Style.picture} />
                </View>
            )}
          </View>
        )}
      </View>
      {pictures.length < 1 ? alertInfo() : null }
  </>);
}

const widthForPicture = (Dimensions.get('window').width / 3);

const Style = StyleSheet.create({
  containerPictures: {
    flex: 1,
    justifyContent: 'flex-start',
    flexDirection: 'column',
    marginTop: 30
  },
  rowPictures: {
    flex: 1,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    marginTop: 2
  },
  carPicture: {
    marginHorizontal: 5,
    marginVertical: 5
  },
  picture: {
    width: widthForPicture - 16,
    height: widthForPicture,
  },
  button: {
    backgroundColor: '#ffffff',
  },
  buttonTitle: {
      color: '#3d3d3d',
      textTransform: 'uppercase'
  }
});

export default ChooseImage;


