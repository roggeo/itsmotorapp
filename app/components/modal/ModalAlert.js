import React from 'react';
import {
    StyleSheet,
    View,
    Text,
} from 'react-native';
import Modal from "react-native-modal";
import {
    Button
} from 'react-native-elements';

const ModalAlert = (props) => {

    const mdlContent = props.children;
    const {
        mdlTitle,
        mdlIsVisible,
        mdlFooterBtnNot,
        mdlFooterBtnYes,
        mdlFooterBtnNotCall,
        mdlFooterBtnYesCall
    } = props;
    
    return (
        <Modal isVisible={mdlIsVisible}>
            <View style={ModalStyle.container}>                
                <Text style={ModalStyle.title}>{mdlTitle}</Text>
                {typeof mdlTitle !== 'string'
                    ? <mdlContent/>
                    : <Text style={ModalStyle.text}>{mdlContent}</Text>
                }
                <View style={ModalStyle.footerButtons}>
                    {!mdlFooterBtnNot ? null :
                        <Button
                            titleStyle={[ModalStyle.footerButtonText, {color: '#666'}]}
                            buttonStyle={[ModalStyle.footerButtonBox, {backgroundColor: '#ffffff'}]}
                            title={mdlFooterBtnNot}
                            onPress={mdlFooterBtnNotCall}/>
                    }
                    {!mdlFooterBtnYes ? null :
                        <Button
                            titleStyle={ModalStyle.footerButtonText}
                            buttonStyle={[ModalStyle.footerButtonBox, {backgroundColor: 'green'}]}
                            title={mdlFooterBtnYes}
                            onPress={mdlFooterBtnYesCall}/>
                    }
                </View>
            </View>
        </Modal>
    );
}

const ModalStyle = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        padding: 20
    },
    title: {
        fontSize: 19,
        textAlign: "center",
        fontWeight: "bold",
        marginBottom: 15
    },
    text: {
        fontSize: 16
    },
    footerButtons: {
        flex:1,
        flexDirection: "row",
        justifyContent: 'flex-end',
        marginBottom:30,
        marginTop: 20
    },
    footerButtonBox: {
        paddingHorizontal: 35,
        paddingVertical: 20
    },
    footerButtonText: {
        fontSize: 17
    }
});

export default ModalAlert;