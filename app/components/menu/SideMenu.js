import React, {useEffect} from 'react';
import {
  ScrollView, 
  Text, 
  View,
  TouchableOpacity
} from 'react-native';
import {NavigationActions} from 'react-navigation';
import SafeAreaView from 'react-native-safe-area-view';
import { Icon } from 'react-native-elements';
import { DarkTheme } from './../../services/ThemeManager';


const navigateToScreen = (props, route) => {
  const navigateAction = NavigationActions.navigate({
    routeName: route
  });
  props.navigation.dispatch(navigateAction);
}

const sideMenuDatas = [
  {name: 'Início', target: 'Home', icon: 'home'},
  {name: 'Mensagens', target: 'Messages' , icon: 'mail'},
  {name: 'Agendamentos', target: 'Schedulings', icon: 'today'},
  {name: 'Meus veículos', target: 'Vehicles' , icon: 'directions-car'},
  {name: 'Meus serviços', target: 'Services', icon: 'build'},
  {name: 'Combustível', target: 'FuelBillList', icon: 'local-gas-station'},
  {name: 'Meus dados', target: 'Account', icon: 'person'},
  //{name: 'Sair', target: 'Example', icon: 'remove-circle'}
];

const CustomDrawerContentComponent = (props) => {

  const { sideMenuStyle, colors } = DarkTheme();

  return (
    <ScrollView style={sideMenuStyle().container}>
      <SafeAreaView
        style={sideMenuStyle().containerSafeArea}
        forceInset={{ top: 'always', horizontal: 'never' }}
      >
        <View>
          {sideMenuDatas.map((val, index) =>
            <TouchableOpacity style={sideMenuStyle().menuLinkRow} key={index} onPress={() => navigateToScreen(props, val.target)}>
              <Icon
                name={val.icon}
                type='material'
                color={colors().colorSideMenuIcon}
                size={20}
              />
              <Text style={sideMenuStyle().menuLinkText}>
                {val.name}
              </Text>
            </TouchableOpacity>
          )}
        </View>
      </SafeAreaView>
    </ScrollView>
  );
}

export default CustomDrawerContentComponent;