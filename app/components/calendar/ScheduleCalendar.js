import React, { useState } from 'react';
import { View } from 'react-native';
import { LocaleConfig, Calendar } from 'react-native-calendars';
import { Overlay } from 'react-native-elements';
import ScheduleDay from './ScheduleDay';


LocaleConfig.locales['pt-BR'] = {
    monthNames: ['JANEIRO','FEVEREIRO','MARÇO','ABRIL','MAIO','JUNHO','JULHO','AGOSTO','SETEMBRO','OUTUBRO','NOVEMBRO','DEZEMBRO'],
    monthNamesShort: ['JAN','FEV','MAR','ABR','MAI','JUN','JUL','AGO','SET','OUT','NOV','DEZ'],
    dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb'],
    today: 'Hoje'
};

LocaleConfig.defaultLocale = 'pt-BR';


const ScheduleCalendar = (props) => {

    const [showModalDay, setShowModalDay] = useState(false);
    const [pressedDay, setPressedDay] =  useState(null);

    const handleOnPressDay = (day) => {
        setPressedDay(day);
        setShowModalDay(true);
    }

    const handleOnCloseModalDay = (d) => {
        setShowModalDay(false);
    }

    return (
        <View>
            <Overlay
                isVisible={showModalDay}
                onBackdropPress={handleOnCloseModalDay}
                windowBackgroundColor="rgba(0, 0, 0, .6)"
                overlayBackgroundColor="white"
                width="auto"
                height="auto"
            >
                <ScheduleDay pressedDay={pressedDay}/>
            </Overlay>            
            <Calendar
            onDayPress={handleOnPressDay}
            markedDates={{
                '2020-02-04': {
                periods: [
                    {startingDay: false, endingDay: true, color: '#5f9ea0'},
                    {startingDay: false, endingDay: true, color: '#ffa500'},
                    {startingDay: true, endingDay: false, color: '#f0e68c'}
                ]
                },
                '2020-02-07': {
                    customStyles: {
                        container: {
                            backgroundColor: 'green'
                        },
                        text: {
                            color: 'white',
                            fontWeight: 'bold'
                        }
                    }
                }
            }}
            // Date marking style [simple/period/multi-dot/custom]. Default = 'simple'
            // markingType='multi-period'
            markingType={'custom'}
            />
        </View>
    );    
}

export default ScheduleCalendar;