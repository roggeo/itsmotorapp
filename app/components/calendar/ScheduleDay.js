import React, { useState } from 'react';
import {
  Text,
  View,
  Picker
} from 'react-native';
import { StyleSheet } from 'react-native';
import { getMonthName } from '../../utils/DateUtils';

const ScheduleDay = (props) => {
  
  const {pressedDay} = props;
  const [defaultHour, setDefaultHour] = useState(pressedDay.day);

  const schedules = [
    {label: '11:00 h', value: '11:00:00'},
    {label: '14:00 h', value: '14:00:00'},
    {label: '15:00 h', value: '15:00:00'},
    {label: '16:00 h', value: '16:00:00'},
    {label: '17:00 h', value: '17:00:00'}
  ]

  const handleOnChange = (itemValue, itemIndex) => {
    setDefaultHour(itemValue);
  }

  return (
      <View>
        <View style={Style.titleContainer}>
          <Text style={Style.title}>Do dia {pressedDay.day} de {getMonthName(pressedDay.month)},</Text>
          <Text style={Style.title}>qual é o melhor horário para você?</Text>
        </View>
        <View>
          <Picker
            style={Style.pickerContainer}
            selectedValue={defaultHour}
            onValueChange={handleOnChange}
            >
            <Picker.Item style={Style.pickerItem} label="Selecione..." value="0" />
            {schedules.map((val, index) =>
              <Picker.Item style={Style.pickerItem} key={index} label={val.label} value={val.value} />
            )}
          </Picker>
        </View>
      </View>
  );
}

const Style = StyleSheet.create({
  titleContainer: {
    marginBottom: 30
  },
  title: {
    fontSize: 18
  },
  pickerContainer: {
    height: 50,
    marginVertical: 20,
  },
  pickerItem: {
    fontSize: 18
  }
});

export default ScheduleDay;