import React, {useState} from 'react';
import {
  View,
  Text,
  Image
} from 'react-native';
import MapView, { Marker } from 'react-native-maps';
import { AssetsImages } from './../../services/AssetsManager';

const GoogleMaps = (props) => {

  const { mapsMarker } = AssetsImages();

  let currentLocation = {
    latitude: -16.3286956,
    longitude: -48.9495158,
    latitudeDelta: 0.0122,
    longitudeDelta: 0.0121,
  }

  let companies = [
    {
      companyName: 'Serviços Automotivos da Bmw',
      companyNickname: 'Bmw Auto',
      companyLat:-16.3286956,
      companyLon:-48.9495158
    },
    {
      companyName: 'Brasil Estética Automotiva',
      companyNickname: 'Auto Estética',
      companyLat:-16.3324572,
      companyLon:-48.9527363
    },
    {
      companyName: 'Ana Som Automotivo',
      companyNickname: 'Anasom',
      companyLat:-16.3246116,
      companyLon:-48.9528209
    }
  ];

  return (
      <View>
        <MapView
          style={{height: 410}}
          initialRegion={currentLocation}>
            {companies.map((val,index) => 
              <Marker
                key={index}
                coordinate={{ latitude: val.companyLat, longitude: val.companyLon }}
                title={val.companyNickname}
                description={val.companyName}>
                <View>                
                  <Image
                    source={mapsMarker()}
                  />
                </View>
            </Marker>
            )}
        </MapView>
    </View>
  );
}

export default GoogleMaps;